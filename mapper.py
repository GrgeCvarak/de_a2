#!/usr/bin/env python3
"""mapper.py"""
import re
import sys
import json
from dataclasses import dataclass

@dataclass
class Tweet:
    id:str
    text: str
    isRetweeted: bool

pattern = re.compile('[\W_]')

idSet = set()

for line in sys.stdin:
    if len(line) > 1 :
        try:
            tweetjson = line[line.index('{'):]
            tweetdict = json.loads(tweetjson)
            tweet = Tweet(str(tweetdict['id']),tweetdict['text'],'retweeted_status' in tweetdict)
            if not tweet.isRetweeted and tweet.id in idSet: print("{}\t{}".format('tduplicate', 1))

            if not tweet.isRetweeted and tweet.id not in idSet: 
                wordfragments = list(map(lambda word:re.sub(pattern, '', word),tweet.text.lower().split()))
                if 'han' in wordfragments : print('\t'.join(['han','1']))
                if 'hon' in wordfragments : print('\t'.join(['hon','1']))
                if 'den' in wordfragments : print('\t'.join(['den','1']))
                if 'det' in wordfragments : print('\t'.join(['det','1']))
                if 'denna' in wordfragments : print('\t'.join(['denna','1']))
                if 'denne' in wordfragments : print('\t'.join(['denne','1']))
                if 'hen' in wordfragments : print('\t'.join(['hen','1']))
                print("{}\t{}".format('tweet', 1))
                idSet.add(tweet.id)
            if tweet.isRetweeted: print("{}\t{}".format('retweet', 1))

        except:
            f = open("log.txt", "w")
            f.write(line)
            f.close()