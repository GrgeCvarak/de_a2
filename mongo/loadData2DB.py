#!/usr/bin/env python3

import tarfile
from pymongo import MongoClient  
import json

myclient = MongoClient("mongodb://localhost:27017/")  
db = myclient["tweets"] 
Collection = db["data"] 


myclient = MongoClient("mongodb://localhost:27017/")  
name = 'tweets.tar.gz'
with tarfile.open(name, "r:gz") as tar:
    for member in tar.getmembers():
        print(member.name)
        if "tweets/tweets_" in member.name:
            with tar.extractfile(member) as f:
                if f is not None:
                    content = f.read()
                    text = content.decode('utf-8')
                    allinall = (len(text.split('\n\n')))
                    counter = 0
                    for line in text.split('\n\n'):
                        counter = counter + 1
                        if counter % 10000 == 0 : print('{}%'.format(round(counter / allinall * 100)))
                        if len(line)>1:
                            file_data = json.loads(line.strip()) 
                            Collection.insert_one(file_data)   
            
    print('kapta halt')
